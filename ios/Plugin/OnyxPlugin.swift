import Foundation
import Capacitor

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(OnyxPlugin)
public class OnyxPlugin: CAPPlugin {
    let PLUGIN_ACTION:String = "action";
    let PLUGIN_ACTION_MATCH:String = "match";
    let PLUGIN_ACTION_CAPTURE:String = "capture";
    let ONYX_LICENSE:String = "onyxLicense";
    let RETURN_RAW_IMAGE:String = "returnRawImage";
    let RETURN_PROCESSED_IMAGE:String = "returnProcessedImage";
    let RETURN_ENHANCED_IMAGE:String = "returnEnhancedImage";
    let RETURN_WSQ:String = "returnWSQ";
    let RETURN_FINGERPRINT_TEMPLATE:String = "returnFingerprintTemplate";
    let WHOLE_FINGER_CROP:String = "wholeFingerCrop";
    let CROP_SIZE:String = "cropSize";
    let CROP_SIZE_WIDTH:String = "width";
    let CROP_SIZE_HEIGHT:String = "height";
    let CROP_FACTOR:String = "cropFactor";
    let SHOW_LOADING_SPINNER:String = "showLoadingSpinner";
    let USE_MANUAL_CAPTURE:String = "useManualCapture";
    let SHOW_MANUAL_CAPTURE_TEXT:String = "showManualCaptureText";
    let MANUAL_CAPTURE_TEXT:String = "manualCaptureText";
    let USE_ONYX_LIVE:String = "useOnyxLive";
    let USE_FLASH:String = "useFlash";
    let RETICLE_ORIENTATION:String = "reticleOrientation";
    let RETICLE_ORIENTATION_LEFT:String = "LEFT";
    let RETICLE_ORIENTATION_RIGHT:String = "RIGHT";
    let RETICLE_ORIENTATION_THUMB_PORTRAIT:String = "THUMB_PORTRAIT";
    let RETICLE_ANGLE:String = "reticleAngle";
    let RETICLE_SCALE:String = "reticleScale";
    let BACKGROUND_COLOR_HEX_STRING:String = "backgroundColorHexString";
    let PROBE:String = "probe";
    let REFERENCE:String = "reference";
    let PYRAMID_SCALES:String = "pyramidScales";
    let BACK_BUTTON_TEXT:String = "backButtonText";
    let INFO_TEXT:String = "infoText";
    let INFO_TEXT_COLOR_HEX_STRING:String = "infoTextColorHexString";
    let BASE64_IMAGE_DATA:String = "base64ImageData";
    
    var mPluginCall:CAPPluginCall? = nil;
    var mPluginAction:String = "";
    
    @objc func exec(_ call: CAPPluginCall) {
        mPluginCall = call;
        mPluginAction = call.getString(PLUGIN_ACTION) ?? "";
        let onyxLicense:String = call.getString(ONYX_LICENSE) ?? "";
        if (mPluginAction.isEmpty) {
            call.reject("Must provide an action to execute");
        }
        if (onyxLicense.isEmpty) {
            call.reject("Must provide an ONYX license key");
        }
        switch mPluginAction {
        case PLUGIN_ACTION_CAPTURE:
            setupOnyx();
            break;
        case PLUGIN_ACTION_MATCH:
            match();
            break;
        default:
            call.reject("Invalid plugin action");
        }
    }
    
    @objc override public func checkPermissions(_ call: CAPPluginCall) {
        call.unimplemented("Not implemented on iOS.")
    }

    @objc override public func requestPermissions(_ call: CAPPluginCall) {
        call.unimplemented("Not implemented on iOS.")
    }
    
    private func match() -> Void {
        
    }
    
    private func setupOnyx() -> Void {
        let onyxLicense:String = mPluginCall?.getString(ONYX_LICENSE) ?? "";
//        let onyxConfigBuilder: OnyxConfigurationBuilder = OnyxConfigurationBuilder();
//        onyxConfigBuilder.setViewController(self.bridge?.viewController)
//            .setLicenseKey(onyxLicense)
//            .setReturnProcessedImage(true)
//            .setSuccessCallback(onyxSuccessCallback)
//            .setErrorCallback(onyxErrorCallback)
//            .setOnyxCallback(onyxCallback)
//            .setReturnRawImage(mPluginCall?.getBool(RETURN_RAW_IMAGE, false) ?? false)
//            .setUseFlash(mPluginCall?.getBool(USE_FLASH, true) ?? true)
//        onyxConfigBuilder.buildOnyxConfiguration();
        
        let onyxConfig: OnyxConfiguration = OnyxConfiguration();
        onyxConfig.viewController = self.bridge?.viewController;
        onyxConfig.licenseKey = onyxLicense;
        onyxConfig.onyxCallback = onyxCallback;
        onyxConfig.successCallback = onyxSuccessCallback;
        onyxConfig.errorCallback = onyxErrorCallback;
        onyxConfig.returnRawFingerprintImage = mPluginCall?.getBool(RETURN_RAW_IMAGE, false) ?? false;
        onyxConfig.returnProcessedFingerprintImage = mPluginCall?.getBool(RETURN_PROCESSED_IMAGE, false) ?? false;
        onyxConfig.returnEnhancedFingerprintImage = mPluginCall?.getBool(RETURN_ENHANCED_IMAGE, false) ?? false;
        onyxConfig.returnWsq = mPluginCall?.getBool(RETURN_WSQ, false) ?? false;
        onyxConfig.returnFingerprintTemplate = mPluginCall?.getBool(RETURN_FINGERPRINT_TEMPLATE, false) ?? false;
        onyxConfig.useFlash = mPluginCall?.getBool(USE_FLASH, true) ?? true;
        onyxConfig.useLiveness = mPluginCall?.getBool(USE_ONYX_LIVE, false) ?? false;
        onyxConfig.wholeFingerCrop = mPluginCall?.getBool(WHOLE_FINGER_CROP, false) ?? false;
        onyxConfig.showSpinner = mPluginCall?.getBool(SHOW_LOADING_SPINNER, false) ?? false;
        onyxConfig.useManualCapture = mPluginCall?.getBool(USE_MANUAL_CAPTURE, false) ?? false;
        onyxConfig.showManualCaptureText = mPluginCall?.getBool(SHOW_MANUAL_CAPTURE_TEXT, true) ?? true;
        onyxConfig.cropFactor = mPluginCall?.getFloat(CROP_FACTOR, 0.8) ?? 0.8;
        
        let backgroundColorHexString: String = mPluginCall?.getString(BACKGROUND_COLOR_HEX_STRING, "") ?? "";
        if (!backgroundColorHexString.isEmpty) {
            onyxConfig.backgroundColorHexString = backgroundColorHexString;
        }

        let backButtonText: String = mPluginCall?.getString(BACK_BUTTON_TEXT, "") ?? "";
        if (!backButtonText.isEmpty) {
            onyxConfig.backButtonText = backButtonText;
        }
        
        let manualCaptureText: String = mPluginCall?.getString(MANUAL_CAPTURE_TEXT, "") ?? "";
        if (!manualCaptureText.isEmpty) {
            onyxConfig.manualCaptureText = manualCaptureText;
        }

        let reticleOrientationString: String = mPluginCall?.getString(RETICLE_ORIENTATION, "") ?? "";
        if (!reticleOrientationString.isEmpty) {
            var reticleOrientation: ReticleOrientation = ReticleOrientation(rawValue: 0);
            switch reticleOrientationString {
            case RETICLE_ORIENTATION_LEFT:
                reticleOrientation = ReticleOrientation(rawValue: 0);
                break;
            case RETICLE_ORIENTATION_RIGHT:
                reticleOrientation = ReticleOrientation(rawValue: 1);
                break;
            case RETICLE_ORIENTATION_THUMB_PORTRAIT:
                reticleOrientation = ReticleOrientation(rawValue: 2);
                break;
            default:
                reticleOrientation = ReticleOrientation(rawValue: 0);
            }
            onyxConfig.reticleOrientation = reticleOrientation;
        }

        let cropSize: [String:JSValue] = mPluginCall?.getObject(CROP_SIZE, [:]) ?? [:];
        if (!cropSize.isEmpty) {
            let width: Int = cropSize[CROP_SIZE_WIDTH] as? Int ?? 512;
            let height: Int = cropSize[CROP_SIZE_HEIGHT] as? Int ?? 300;
            onyxConfig.cropSize = CGSize(width: width, height: height);
        }
        
        let onyx: Onyx = Onyx()
        onyx.doSetup(onyxConfig)
    }
    
    func onyxCallback(configuredOnyx: Onyx?) -> Void {
        NSLog("Onyx Callback");
        DispatchQueue.main.async {
            configuredOnyx!.capture(self.bridge?.viewController);
        }
    }
    
    func onyxSuccessCallback(onyxResult: OnyxResult?) -> Void {
        NSLog("Onyx Success Callback");
        var onyxResults: Array<[String:Any]> = [[String:Any]()];
        var rawFigerprintImages: NSMutableArray = [];
        var processedFingerprintImages: NSMutableArray = [];
        var enhancedFingerprintImages: NSMutableArray = [];
        var wsqDataArray: NSMutableArray = [];
        var fingerprintTemplates: NSMutableArray = [];
        var numberFingersProcessed: Int = 0;
        
        if (nil != onyxResult!.getRawFingerprintImages()) {
            rawFigerprintImages = onyxResult!.getRawFingerprintImages();
            if (rawFigerprintImages.count > numberFingersProcessed) {
                numberFingersProcessed = rawFigerprintImages.count
            }
        }
        if (nil != onyxResult!.getProcessedFingerprintImages()) {
            processedFingerprintImages = onyxResult!.getProcessedFingerprintImages();
            if (processedFingerprintImages.count > numberFingersProcessed) {
                numberFingersProcessed = processedFingerprintImages.count
            }
        }
        if (nil != onyxResult!.getEnhancedFingerprintImages()) {
            enhancedFingerprintImages = onyxResult!.getEnhancedFingerprintImages();
            if (enhancedFingerprintImages.count > numberFingersProcessed) {
                numberFingersProcessed = enhancedFingerprintImages.count
            }
        }
        if (nil != onyxResult!.getWsqData()) {
            wsqDataArray = onyxResult!.getWsqData();
            if (wsqDataArray.count > numberFingersProcessed) {
                numberFingersProcessed = wsqDataArray.count
            }
        }
        if (nil != onyxResult!.getFingerprintTemplates()) {
            fingerprintTemplates = onyxResult!.getFingerprintTemplates();
            if (fingerprintTemplates.count > numberFingersProcessed) {
                numberFingersProcessed = fingerprintTemplates.count
            }
        }
        var keysArray: Array<String>;
        var valuesArray: Array<Any>;
        var i = 0;
        repeat {
            i+=1;
            var rawImageUri: String = "";
            var processedImageUri: String = "";
            var enhancedImageUri: String = "";
            var base64EncodedWsq: String = "";
            var base64EncodedFingerprintTemplate: String = "";
            var captureMetrics: Dictionary = [String:Any]();
            
            if (mPluginCall?.getBool(RETURN_RAW_IMAGE) ?? false) {
                if (rawFigerprintImages.count == numberFingersProcessed) {
                    rawImageUri = getFingerprintImageUri(fingerprintImage: rawFigerprintImages[i] as! UIImage);
                }
            }
            if (mPluginCall?.getBool(RETURN_PROCESSED_IMAGE) ?? false) {
                if (processedFingerprintImages.count == numberFingersProcessed) {
                    processedImageUri = getFingerprintImageUri(fingerprintImage: processedFingerprintImages[i] as! UIImage);
                }
            }
            if (mPluginCall?.getBool(RETURN_ENHANCED_IMAGE) ?? false) {
                if (enhancedFingerprintImages.count == numberFingersProcessed) {
                    enhancedImageUri = getFingerprintImageUri(fingerprintImage: enhancedFingerprintImages[i] as! UIImage);
                }
            }
            if (mPluginCall?.getBool(RETURN_WSQ) ?? false) {
                if (wsqDataArray.count == numberFingersProcessed) {
                    base64EncodedWsq = getBase64EncodedString(data: wsqDataArray[i] as! NSData);
                }
            }
            if (mPluginCall?.getBool(RETURN_FINGERPRINT_TEMPLATE) ?? false) {
                if (fingerprintTemplates.count == numberFingersProcessed) {
                    base64EncodedFingerprintTemplate = getBase64EncodedString(data: fingerprintTemplates[i] as! NSData);
                }
            }
            if (nil != onyxResult!.getMetrics()) {
                var nfiqMetricsJson: Dictionary = [String:Any]();
                let metrics: CaptureMetrics = onyxResult!.getMetrics();
                var nfiqScore: Int32 = 0;
                var mlpScore: Float = 0;
                let focusQuality: Float = metrics.getFocusQuality();
                let livenessConfidence: Float = metrics.getLivenessConfidence();
                if (nil != metrics.getNfiqMetrics()) {
                    let nfiqMetricsArray: NSMutableArray = metrics.getNfiqMetrics();
                    if (nfiqMetricsArray.count == numberFingersProcessed) {
                        let nfiqMetrics: NfiqMetrics = nfiqMetricsArray[i] as! NfiqMetrics;
                        nfiqScore = nfiqMetrics.getNfiqScore();
                        mlpScore = nfiqMetrics.getMlpScore();
                        keysArray = ["nfiqScore", "mlpScore"];
                        valuesArray = [nfiqScore, mlpScore];
                        nfiqMetricsJson = Dictionary(uniqueKeysWithValues: zip(keysArray, valuesArray));
                    }
                }
                keysArray = ["livenessConfidence", "focusQuality", "nfiqMetrics"];
                valuesArray = [livenessConfidence, focusQuality, nfiqMetricsJson];
                captureMetrics = Dictionary(uniqueKeysWithValues: zip(keysArray, valuesArray));
            }
            keysArray = [
                "rawFingerprintDataUri",
                "processedFingerprintDataUri",
                "enhancedFingerprintDataUri",
                "base64EncodedWsqBytes",
                "base64EncodedFingerprintTemplate",
                "captureMetrics"
            ];
            valuesArray = [
                rawImageUri,
                processedImageUri,
                enhancedImageUri,
                base64EncodedWsq,
                base64EncodedFingerprintTemplate,
                captureMetrics
            ];
            let iOnyxResult = Dictionary(uniqueKeysWithValues: zip(keysArray, valuesArray));
            onyxResults[i] = iOnyxResult;
        }
        while (i < numberFingersProcessed);
        
        keysArray = [PLUGIN_ACTION, "onyxResults"];
        valuesArray = [mPluginAction, onyxResults];
        let result: Dictionary = Dictionary(uniqueKeysWithValues: zip(keysArray, valuesArray));
        
        mPluginCall?.resolve(result);
    }
    
    func onyxErrorCallback(onyxError: OnyxError?) -> Void {
        NSLog("Onyx Error Callback");
        let errorMessage = onyxError?.errorMessage ?? "Onyx returned an error";
        mPluginCall?.reject(errorMessage);
    }
    
    func getFingerprintImageUri(fingerprintImage: UIImage) -> String {
        if let imageData: Data = fingerprintImage.jpegData(compressionQuality: 1.0) {
            return "\(IMAGE_URI_PREFIX)\(imageData.base64EncodedString(options: []))";
        } else {
            return "";
        }
    }
    
    func getBase64EncodedString(data: NSData) -> String {
        return data.base64EncodedString(options: [])
    }
}
