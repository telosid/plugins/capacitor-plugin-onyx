import { WebPlugin } from '@capacitor/core';
import type { IOnyxConfiguration, IOnyxPluginResult } from '@telosid/onyx-typedefs';

import type { OnyxPlugin, PermissionStatus } from './definitions';

export class OnyxWeb extends WebPlugin implements OnyxPlugin {
  async exec(options: IOnyxConfiguration): Promise<IOnyxPluginResult> {
    console.log('ECHO', options);
    throw this.unimplemented('Not implemented on web.');
  }

  async checkPermissions(): Promise<PermissionStatus> {
    throw this.unimplemented('Not implemented on web.');
  }

  async requestPermissions(): Promise<PermissionStatus> {
    throw this.unimplemented('Not implemented on web.');
  }
}
