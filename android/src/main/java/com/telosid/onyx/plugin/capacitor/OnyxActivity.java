package com.telosid.onyx.plugin.capacitor;

import static com.dft.onyxcamera.config.OnyxConfiguration.ErrorCallback.Error.AUTOFOCUS_FAILURE;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.widget.LinearLayout;

import com.dft.onyx.FingerprintTemplate;
import com.dft.onyx.NfiqMetrics;
import com.dft.onyxcamera.config.Onyx;
import com.dft.onyxcamera.config.OnyxConfiguration;
import com.dft.onyxcamera.config.OnyxConfigurationBuilder;
import com.dft.onyxcamera.config.OnyxError;
import com.dft.onyxcamera.config.OnyxResult;
import com.dft.onyxcamera.ui.CaptureMetrics;
import com.dft.onyxcamera.ui.reticles.Reticle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class OnyxActivity extends Activity {
  private static final String TAG = OnyxActivity.class.getSimpleName();
  private static final String ONYX_BLUE_HEX_STRING = "#3698D3";
  private Activity mActivity;
  private Context mContext;
  private Onyx mOnyx;
  private boolean mUseManualCapture = false;
  private boolean mIsProcessingManualCapture = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mActivity = this;
    mContext = this;
    requestWindowFeature(Window.FEATURE_NO_TITLE);

    int backgroundColor = Color.parseColor(ONYX_BLUE_HEX_STRING);
    try {
      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.BACKGROUND_COLOR_HEX_STRING.getKey())) {
        String backgroundColorHexString = OnyxPlugin.mArgs.getString(
                OnyxPlugin.OnyxConfig.BACKGROUND_COLOR_HEX_STRING.getKey());
        if (!backgroundColorHexString.isEmpty()) {
          backgroundColor = Color.parseColor(backgroundColorHexString);
        }
      }
    } catch (JSONException e) {
      String errorMessage = "Failed to set JSON key value pair: " + e.getMessage();
      onError(errorMessage);
    } catch (IllegalArgumentException e) {
      String errorMessage = "Unable to parse background color: " + e.getMessage();
      onError(errorMessage);
    }

    LinearLayout rootView = new LinearLayout(mContext);
    rootView.setOrientation(LinearLayout.VERTICAL);
    LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
    );
    rootView.setLayoutParams(llp);
    rootView.setBackgroundColor(backgroundColor);

    setContentView(rootView);
    setupOnyx();
  }

  @Override
  protected void onResume() {
    super.onResume();
  }

  private void setupUI() {

  }

  private void setupOnyx() {
    try {
      // Create an OnyxConfigurationBuilder and configure it with desired options
      String onyxLicense = OnyxPlugin.mArgs.getString(
              OnyxPlugin.OnyxConfig.ONYX_LICENSE.getKey());

      OnyxConfigurationBuilder onyxConfigurationBuilder = new OnyxConfigurationBuilder()
              .setActivity(mActivity)
              .setLicenseKey(onyxLicense)
              .setSuccessCallback(onyxSuccessCallback)
              .setErrorCallback(new OnyxConfiguration.ErrorCallback() {
                @Override
                public void onError(OnyxError error) {
                  Log.e(TAG, error.toString());
                  if (error.error != AUTOFOCUS_FAILURE) {
                    OnyxPlugin.onError(error.errorMessage);
                    finish();
                  } else {
                    mActivity.runOnUiThread(new Runnable() {
                      public void run() {
                        if (!mUseManualCapture) {
                          mOnyx.capture();
                        }
                      }
                    });
                  }
                }
              })
              .setOnyxCallback(new OnyxConfiguration.OnyxCallback() {
                @Override
                public void onConfigured(final Onyx configuredOnyx) {
                  mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                      mOnyx = configuredOnyx;
                      mOnyx.create(mActivity);
                      if (!mUseManualCapture) {
                        mOnyx.capture();
                      }
                    }
                  });
                }
              });

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_RAW_IMAGE.getKey())) {
        onyxConfigurationBuilder.setReturnRawImage(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_RAW_IMAGE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_PROCESSED_IMAGE.getKey())) {
        onyxConfigurationBuilder.setReturnProcessedImage(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_PROCESSED_IMAGE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_ENHANCED_IMAGE.getKey())) {
        onyxConfigurationBuilder.setReturnEnhancedImage(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_ENHANCED_IMAGE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_WSQ.getKey())) {
        onyxConfigurationBuilder.setReturnWSQ(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_WSQ.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_FINGERPRINT_TEMPLATE.getKey())) {
        onyxConfigurationBuilder.setReturnFingerprintTemplate(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_FINGERPRINT_TEMPLATE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.SHOULD_CONVERT_TO_ISO_TEMPLATE.getKey())) {
        onyxConfigurationBuilder.setShouldConvertToISOTemplate(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.SHOULD_CONVERT_TO_ISO_TEMPLATE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.COMPUTE_NFIQ_METRICS.getKey())) {
        onyxConfigurationBuilder.setComputeNfiqMetrics(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.COMPUTE_NFIQ_METRICS.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.CROP_SIZE.getKey())) {
        JSONObject cropSize = OnyxPlugin.mArgs.getJSONObject(
                OnyxPlugin.OnyxConfig.CROP_SIZE.getKey());
        int width = 512;
        int height = 300;
        if (cropSize.has(OnyxPlugin.OnyxConfig.CROP_SIZE_WIDTH.getKey())) {
          String cropSizeWidthString = cropSize.getString(
                  OnyxPlugin.OnyxConfig.CROP_SIZE_WIDTH.getKey());
          if (!cropSizeWidthString.isEmpty()) {
            width = Integer.parseInt(cropSizeWidthString);
          }
        }
        if (cropSize.has(OnyxPlugin.OnyxConfig.CROP_SIZE_HEIGHT.getKey())) {
          String cropSizeHeightString = cropSize.getString(
                  OnyxPlugin.OnyxConfig.CROP_SIZE_HEIGHT.getKey());
          if (!cropSizeHeightString.isEmpty()) {
            height = Integer.parseInt(cropSizeHeightString);
          }
        }
        onyxConfigurationBuilder.setCropSize(width, height);
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.CROP_FACTOR.getKey())) {
        String cropFactorString = OnyxPlugin.mArgs.getString(
                OnyxPlugin.OnyxConfig.CROP_FACTOR.getKey());
        if (!cropFactorString.isEmpty()) {
          onyxConfigurationBuilder.setCropFactor(Double.parseDouble(cropFactorString));
        }
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.SHOW_LOADING_SPINNER.getKey())) {
        onyxConfigurationBuilder.setShowLoadingSpinner(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.SHOW_LOADING_SPINNER.getKey()));
      }

      if (mUseManualCapture) {
        onyxConfigurationBuilder.setUseManualCapture(mUseManualCapture);
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.USE_ONYX_LIVE.getKey())) {
        onyxConfigurationBuilder.setUseOnyxLive(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.USE_ONYX_LIVE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.USE_FLASH.getKey())) {
        onyxConfigurationBuilder.setUseFlash(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.USE_FLASH.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETICLE_ORIENTATION.getKey())) {
        String reticleOrientationString = OnyxPlugin.mArgs.getString(
                OnyxPlugin.OnyxConfig.RETICLE_ORIENTATION.getKey());
        if (!reticleOrientationString.isEmpty()) {
          Reticle.Orientation reticleOrientation = Reticle.Orientation.LEFT;
          if (reticleOrientationString.equalsIgnoreCase(
                  OnyxPlugin.OnyxConfig.RETICLE_ORIENTATION_LEFT.getKey())) {
            reticleOrientation = Reticle.Orientation.LEFT;
          } else if (reticleOrientationString.equalsIgnoreCase(
                  OnyxPlugin.OnyxConfig.RETICLE_ORIENTATION_RIGHT.getKey())) {
            reticleOrientation = Reticle.Orientation.RIGHT;
          } else if (reticleOrientationString.equalsIgnoreCase(
                  OnyxPlugin.OnyxConfig.RETICLE_ORIENTATION_THUMB_PORTRAIT.getKey())) {
            reticleOrientation = Reticle.Orientation.THUMB_PORTRAIT;
          }
          onyxConfigurationBuilder.setReticleOrientation(reticleOrientation);
        }
      }

      // Finally, build the OnyxConfiguration
      onyxConfigurationBuilder.buildOnyxConfiguration();
    } catch (JSONException e) {
      onError("JSONException: " + e.getMessage());
    }
  }

  private OnyxConfiguration.SuccessCallback onyxSuccessCallback = new OnyxConfiguration.SuccessCallback() {
    @Override
    public void onSuccess(OnyxResult onyxResult) {
      JSONObject result = new JSONObject();
      ArrayList<JSONObject> onyxResults = new ArrayList<JSONObject>();
      ArrayList<Bitmap> rawFingerprintImages = null;
      ArrayList<Bitmap> processedFingerprintImages = null;
      ArrayList<Bitmap> enhancedFingerprintImages = null;
      ArrayList<byte[]> wsqDataArrayList = null;
      ArrayList<FingerprintTemplate> fingerprintTemplates = null;
      int numberFingersProcessed = 0;
      try {
        if (null != onyxResult.getRawFingerprintImages()) {
          rawFingerprintImages = onyxResult.getRawFingerprintImages();
          if (rawFingerprintImages.size() > numberFingersProcessed) {
            numberFingersProcessed = rawFingerprintImages.size();
          }
        }
        if (null != onyxResult.getProcessedFingerprintImages()) {
          processedFingerprintImages = onyxResult.getProcessedFingerprintImages();
          if (processedFingerprintImages.size() > numberFingersProcessed) {
            numberFingersProcessed = processedFingerprintImages.size();
          }
        }
        if (null != onyxResult.getEnhancedFingerprintImages()) {
          enhancedFingerprintImages = onyxResult.getEnhancedFingerprintImages();
          if (enhancedFingerprintImages.size() > numberFingersProcessed) {
            numberFingersProcessed = enhancedFingerprintImages.size();
          }
        }
        if (null != onyxResult.getWsqData()) {
          wsqDataArrayList = onyxResult.getWsqData();
          if (wsqDataArrayList.size() > numberFingersProcessed) {
            numberFingersProcessed = wsqDataArrayList.size();
          }
        }
        if (null != onyxResult.getFingerprintTemplates()) {
          fingerprintTemplates = onyxResult.getFingerprintTemplates();
          if (fingerprintTemplates.size() > numberFingersProcessed) {
            numberFingersProcessed = fingerprintTemplates.size();
          }
        }

        Log.v(TAG, "numberFingersProcessed: " + numberFingersProcessed);
        for (int i = 0; i < numberFingersProcessed; i++) {
          JSONObject iOnyxResult = new JSONObject();
          JSONObject captureMetrics = new JSONObject();
          String rawFingerprintDataUri = null;
          String processedFingerprintDataUri = null;
          String enhancedFingerprintDataUri = null;
          String base64EncodedWsqBytes = null;
          String base64EncodedFingerprintTemplate = null;
          if (null != rawFingerprintImages &&
                  rawFingerprintImages.size() == numberFingersProcessed) {
            rawFingerprintDataUri = getDataUriFromBitmap(
                    rawFingerprintImages.get(i).copy(Bitmap.Config.RGB_565, false));
            iOnyxResult.put("rawFingerprintDataUri", rawFingerprintDataUri);
          }
          if (null != processedFingerprintImages &&
                  processedFingerprintImages.size() == numberFingersProcessed) {
            processedFingerprintDataUri = getDataUriFromBitmap(
                    processedFingerprintImages.get(i).copy(Bitmap.Config.RGB_565, false));
            iOnyxResult.put("processedFingerprintDataUri", processedFingerprintDataUri);
          }
          if (null != enhancedFingerprintImages &&
                  enhancedFingerprintImages.size() == numberFingersProcessed) {
            enhancedFingerprintDataUri = getDataUriFromBitmap(
                    enhancedFingerprintImages.get(i).copy(Bitmap.Config.RGB_565, false));
            iOnyxResult.put("enhancedFingerprintDataUri", enhancedFingerprintDataUri);
          }
          if (null != wsqDataArrayList &&
                  wsqDataArrayList.size() == numberFingersProcessed) {
            base64EncodedWsqBytes = Base64.encodeToString(wsqDataArrayList.get(i), Base64.NO_WRAP)
                    .trim();
            iOnyxResult.put("base64EncodedWsqBytes", base64EncodedWsqBytes);
          }
          if (null != fingerprintTemplates &&
                  fingerprintTemplates.size() == numberFingersProcessed) {
            base64EncodedFingerprintTemplate = Base64.encodeToString(
                            fingerprintTemplates.get(i).getData(), Base64.NO_WRAP)
                    .trim();
            iOnyxResult.put("base64EncodedFingerprintTemplate",
                    base64EncodedFingerprintTemplate);
          }
          if (null != onyxResult.getMetrics()) {
            CaptureMetrics metrics = onyxResult.getMetrics();
            captureMetrics.put("focusQuality", metrics.getFocusQuality());
            captureMetrics.put("livenessConfidence", metrics.getLivenessConfidence());
            JSONObject nfiqMetrics = new JSONObject();
            Log.v(TAG, "getNfiqMetrics().size(): " + metrics.getNfiqMetrics());

            if (null != metrics.getNfiqMetrics() &&
                    metrics.getNfiqMetrics().size() == numberFingersProcessed) {
              List<NfiqMetrics> nfiqMetricsList = metrics.getNfiqMetrics();
              nfiqMetrics.put("nfiqScore", nfiqMetricsList.get(i).getNfiqScore());
              nfiqMetrics.put("mlpScore", nfiqMetricsList.get(i).getMlpScore());
            }
            captureMetrics.put("nfiqMetrics", nfiqMetrics);
          }
          iOnyxResult.put("captureMetrics", captureMetrics);
          onyxResults.add(iOnyxResult);
        }
        result.put("onyxResults", new JSONArray(onyxResults.toArray()));
      } catch (JSONException e) {
        String errorMessage = "Failed to set JSON key value pair: " + e.getMessage();
        onError(errorMessage);
      }

      OnyxPlugin.onFinished(Activity.RESULT_OK, result);
      finish();
    }
  };

  private String getDataUriFromBitmap(Bitmap bitmap) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
    byte[] imageBytes = stream.toByteArray();
    bitmap.recycle();
    String dataUri;
    String encodedBytes = Base64.encodeToString(imageBytes, Base64.NO_WRAP).trim();
    dataUri = OnyxPlugin.IMAGE_URI_PREFIX + encodedBytes;
    return dataUri;
  }

  private void onError(String errorMessage) {
    OnyxPlugin.onError(errorMessage);
    finish();
  }
}
