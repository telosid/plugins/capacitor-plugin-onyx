package com.telosid.onyx.plugin.capacitor;

import com.getcapacitor.Bridge;
import com.getcapacitor.BridgeActivity;
import com.getcapacitor.JSObject;
import com.getcapacitor.PermissionState;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.ActivityCallback;
import com.getcapacitor.annotation.CapacitorPlugin;
import com.getcapacitor.annotation.Permission;
import com.getcapacitor.annotation.PermissionCallback;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import com.dft.onyx.FingerprintTemplate;

import java.util.Objects;

import androidx.activity.result.ActivityResult;

@CapacitorPlugin(
        name = "Onyx",
        permissions = {
                @Permission(
                        alias = "camera",
                        strings = {Manifest.permission.CAMERA}
                )
        }
)
public class OnyxPlugin extends Plugin implements OnyxMatch.MatchResultCallback {

  public static final String TAG = "OnyxPlugin";
  public static final String IMAGE_URI_PREFIX = "data:image/jpeg;base64,";
  public static String mPackageName;
  public static JSONObject mArgs;
  private static String mExecuteAction;
  public static OnyxPluginAction mOnyxPluginAction;

  public enum OnyxPluginAction {
    CAPTURE("capture"),
    MATCH("match");
    private final String key;

    OnyxPluginAction(String key) {
      this.key = key;
    }

    public String getKey() {
      return this.key;
    }
  }

  public enum OnyxConfig {
    ACTION("action"),
    ONYX_LICENSE("onyxLicense"),
    RETURN_RAW_IMAGE("returnRawImage"),
    RETURN_PROCESSED_IMAGE("returnProcessedImage"),
    RETURN_ENHANCED_IMAGE("returnEnhancedImage"),
    RETURN_WSQ("returnWSQ"),
    RETURN_FINGERPRINT_TEMPLATE("returnFingerprintTemplate"),
    SHOULD_CONVERT_TO_ISO_TEMPLATE("shouldConvertToISOTemplate"),
    COMPUTE_NFIQ_METRICS("computeNfiqMetrics"),
    CROP_SIZE("cropSize"),
    CROP_SIZE_WIDTH("width"),
    CROP_SIZE_HEIGHT("height"),
    CROP_FACTOR("cropFactor"),
    SHOW_LOADING_SPINNER("showLoadingSpinner"),
    USE_MANUAL_CAPTURE("useManualCapture"),
    USE_ONYX_LIVE("useOnyxLive"),
    USE_FLASH("useFlash"),
    RETICLE_ORIENTATION("reticleOrientation"),
    RETICLE_ORIENTATION_LEFT("LEFT"),
    RETICLE_ORIENTATION_RIGHT("RIGHT"),
    RETICLE_ORIENTATION_THUMB_PORTRAIT("THUMB_PORTRAIT"),
    RETICLE_ANGLE("reticleAngle"),
    BACKGROUND_COLOR_HEX_STRING("backgroundColorHexString"),
    SHOW_BACK_BUTTON("showBackButton"),
    SHOW_MANUAL_CAPTURE_TEXT("showManualCaptureText"),
    MANUAL_CAPTURE_TEXT("manualCaptureText"),
    BACK_BUTTON_TEXT("backButtonText"),
    REFERENCE("reference"),
    PROBE("probe"),
    PYRAMID_SCALES("pyramidScales");
    private final String key;

    OnyxConfig(String key) {
      this.key = key;
    }

    public String getKey() {
      return this.key;
    }
  }

  public static PluginCall mPluginCall;


  @PluginMethod()
  public void exec(PluginCall call) {
    mPluginCall = call;
    Log.v(TAG, "mPluginCall: " + mPluginCall);
    mArgs = mPluginCall.getData();

    if (!mArgs.has(OnyxConfig.ACTION.getKey()) ||
            Objects.requireNonNull(mPluginCall.getString(OnyxConfig.ACTION.getKey(), "")).isEmpty()) {
      onError("Must provide an action to execute");
      return;
    }
    if (!mArgs.has(OnyxConfig.ONYX_LICENSE.getKey()) ||
            Objects.requireNonNull(mPluginCall.getString(OnyxConfig.ONYX_LICENSE.getKey(), "")).isEmpty()) {
      onError("Must provide an ONYX license key");
      return;
    }
    mExecuteAction = call.getString(OnyxConfig.ACTION.getKey(), "");
    Log.v(TAG, "OnyxPlugin action: " + mExecuteAction);

    if (mExecuteAction.equalsIgnoreCase(OnyxPluginAction.MATCH.getKey())) {
      mOnyxPluginAction = OnyxPluginAction.MATCH;
    } else if (mExecuteAction.equalsIgnoreCase(OnyxPluginAction.CAPTURE.getKey())) {
      mOnyxPluginAction = OnyxPluginAction.CAPTURE;
    }

    if (null != mOnyxPluginAction) {
      switch (mOnyxPluginAction) {
        case MATCH:
          try {
            doMatch();
          } catch (JSONException e) {
            onError(e.getMessage());
          }
          break;
        case CAPTURE:
          checkCameraPermissions();
          break;
      }
    } else {
      onError("Invalid plugin action");
    }
  }

  private void checkCameraPermissions() {
    if (getPermissionState("camera") != PermissionState.GRANTED) {
      requestPermissionForAlias("camera", mPluginCall, "cameraPermissionsCallback");
    } else {
      launchOnyx();
    }
  }

  @PermissionCallback
  private void cameraPermissionsCallback(PluginCall call) {
    if (getPermissionState("camera") == PermissionState.GRANTED) {
      launchOnyx();
    } else {
      onError("Camera permissions are required to use Onyx");
    }
  }

  private void launchOnyx() {
    Intent onyxIntent = new Intent(getContext(), OnyxActivity.class);
    onyxIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    getActivity().startActivity(onyxIntent);
  }

  private void doMatch() throws JSONException {
    // Get values for JSON keys
    String encodedReference = mArgs.getString(OnyxConfig.REFERENCE.getKey());
    String encodedProbe = mArgs.getString(OnyxConfig.PROBE.getKey());
    JSONArray scalesJSONArray = null;
    if (mArgs.has(OnyxConfig.PYRAMID_SCALES.getKey())) {
      scalesJSONArray = mPluginCall.getArray(OnyxConfig.PYRAMID_SCALES.getKey());
    }

    // Decode reference fingerprint template data
    byte[] referenceBytes = Base64.decode(encodedReference, Base64.NO_WRAP);

    // Get encoded probe processed fingerprint image data from image URI
    String encodedProbeDataString = encodedProbe.substring(IMAGE_URI_PREFIX.length(), encodedProbe.length());

    // Decode probe probe image data
    byte[] probeBytes = Base64.decode(encodedProbeDataString, Base64.NO_WRAP);

    // Create a bitmap from the probe bytes
    Bitmap probeBitmap = BitmapFactory.decodeByteArray(probeBytes, 0, probeBytes.length);

    // Create a mat from the bitmap
    Mat matProbe = new Mat();
    Utils.bitmapToMat(probeBitmap, matProbe);
    Imgproc.cvtColor(matProbe, matProbe, Imgproc.COLOR_RGB2GRAY);

    // Create reference fingerprint template from bytes
    FingerprintTemplate ftRef = new FingerprintTemplate(referenceBytes, 0);

    // Convert pyramid scales from JSON array to double array
    double[] argsScales = null;
    if (null != scalesJSONArray && scalesJSONArray.length() > 0) {
      argsScales = new double[scalesJSONArray.length()];
      for (int i = 0; i < argsScales.length; i++) {
        argsScales[i] = Double.parseDouble(scalesJSONArray.optString(i));
      }
    }
    final double[] pyramidScales = argsScales;

    OnyxMatch matchTask = new OnyxMatch(getContext(), OnyxPlugin.this);
    matchTask.execute(ftRef, matProbe, pyramidScales);
  }

  @Override
  public void onMatchFinished(boolean match, float score) {
    JSObject result = new JSObject();
    result.put("isVerified", match);
    result.put("matchScore", score);
    onFinished(Activity.RESULT_OK, result);
  }

  public static void onFinished(int resultCode, JSONObject result) {
    if (resultCode == Activity.RESULT_OK) {

      try {
        result.put(OnyxConfig.ACTION.getKey(), mExecuteAction);
        mPluginCall.resolve(new JSObject(result.toString()));
      } catch (JSONException e) {
        String errorMessage = "Failed to set JSON key value pair: " + e.getMessage();
        mPluginCall.reject(errorMessage);
      }
      Log.v(TAG, "result: " + result.toString());
    } else if (resultCode == Activity.RESULT_CANCELED) {
      mPluginCall.reject("Cancelled");
    }
  }

  public static void onError(String errorMessage) {
    Log.e(TAG, errorMessage);
    mPluginCall.reject(errorMessage);
  }
}
